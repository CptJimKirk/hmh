# Handmade Hero

Handmade Hero is a video series where Casey Muratori creates a game engine from scratch step-by-step, with loads of additional educational content along the way. 

I am not affiliated with Casey Muratori or Handmande Hero in anyway, for more information please visit the official website. 
https://handmadehero.org/

# Purpose

This repository is a personal exploration in adapting Whitney-style C idioms in a C context while following along with the series myself. 

# Using this source

`my.h` defines many Whitney-style macros, and the remainder of the application will use those macros. It is recommended to open `my.h` in your editor, import into your own test file, and experiment with the definitions. 

In order to read the macros, it is important to remember C-syntax rules still apply. Macros only represent text replacement. There is no magic. Try a few beta-reductions on test arguments, writing them out by hand, or in your editor.

When trying to understand a single macro construct, such as the for loop constructs `i` and `j` and their parent construct `ij`, it is useful to use the following 3 steps:

1. Exploration
   Try to call the macro. See what errors it gives you, try to make some inferences by the textual pattern
2. Experimentation
   You've gotten it to work, you don't know why, what if you try it this way? Make observations and test the boundaries 
3. Expansion
   Expand out the macro mentally, realizing what each input argument represents in the final expression, arrive at understanding

```C

#define ij(i,n,...) for(TY(n)n_=(n),i=0;i<n_;++i){__VA_ARGS__;}


// FIRST transformation - exploration, intuition
// we see n is being assigned, and i is being passed in
ij(x, 5, ???)

//ok, the .... is multiple forms, and __VA_ARGS__ ends with ;, so a statement?
ij(x, 5, printf("%d\n", x * 2));


// SECOND transformation - experimentation, boundary checking
// x is the counter, interesting, so i can be any token...
ij(numberCounterToken, 5, printf("%d\n", x * 2));

// so x doesn't exist
ij(numberCounterToken, 5, printf("%d\n", numberCounterToken * 2));

// well what about when I need multiple statements?
int a = 4;
ij(x, 5, int b = 2 * x, printf("%d", a + b + x), a++);

// hmmm, that doesn't seem to work

int a = 4;
ij(x, 5, int b = 2 * x; printf("%d", a + b + x); a++);


// THIRD transformation - expansion, final result
// so this means that the form ultimately expands to
// #define ij(i,n,...) for(TY(n)n_=(n),i=0;i<n_;++i){__VA_ARGS__;}

for((typeof n) n_ = (value of n), token = 0; 
    token < n_; ++token) {
    statement1;
    statement2;
    //...
    statementn;
}


// in the last example 
// typeof 5 is int
int a = 4;
for(int n_ = (5), x = 0;
    x < n_;
    ++n){
    int b = 2 * x;
    printf("%d", a + b = x);
    ++a; // where the final semicolon is inserted
}

```


## A working Example

```C
#include <stdio.h> 
#include "my.h"

int main()
{

    int a = 0;
    i(5, // do this 5 times
      int b = 0;
      j(5, // do this 5 times
        printf("i:%d a:%d j:%d b:%d\n", a, b, i, j);
        ++b); // this closes the j macro. notice no ++b; semicolon, because that is inserted by the macro
      ++a);

    printf("final a: %d\n", a);
    return(0);
}
```


