#include <windows.h>
#include <stdint.h>
#include <xinput.h>
#include "my.h"
#include <dsound.h>

typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

#define INTERNAL static
#define LOCAL    static
#define GLOBAL   static

struct win32_offscreen_buffer {
  BITMAPINFO  Info;
  V          *Memory;
  I           Width;
  I           Height;
  I           Pitch;
};

struct win32_window_dimension{
  I Width;
  I Height;

};

GLOBAL B Running;
GLOBAL win32_offscreen_buffer GlobalBackBuffer;
GLOBAL LPDIRECTSOUNDBUFFER SecondaryBuffer;
#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE *pState)
typedef X_INPUT_GET_STATE(x_input_get_state);
X_INPUT_GET_STATE(XInputGetStateStub) {R(ERROR_DEVICE_NOT_CONNECTED);}
GLOBAL x_input_get_state *XInputGetState_ = XInputGetStateStub;

#define X_INPUT_SET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration)
typedef X_INPUT_SET_STATE(x_input_set_state);
X_INPUT_SET_STATE(XInputSetStateStub) {R(ERROR_DEVICE_NOT_CONNECTED);}
GLOBAL x_input_set_state *XInputSetState_ = XInputSetStateStub;

#define XInputGetState XInputGetState_
#define XInputSetState XInputSetState_

#define DIRECT_SOUND_CREATE(name) DWORD WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND* ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

INTERNAL V Win32LoadXInput(V) {
  /* Load the function definitions from the xinput dll of the available version */
  HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");

  $(!XInputLibrary,
    HMODULE XInputLibrary = LoadLibraryA("xinput1_3.dll"));

  $(XInputLibrary,
    XInputGetState = (x_input_get_state *)GetProcAddress(XInputLibrary,"XInputGetState");
    XInputSetState = (x_input_set_state *)GetProcAddress(XInputLibrary,"XInputSetState"));
}

INTERNAL V Win32InitDSound(HWND Window, int32 SamplesPerSec, int32 BufferSize) {
  HMODULE DSoundLibrary = LoadLibraryA("dsound.dll");
  $(DSoundLibrary,
    direct_sound_create* DirectSoundCreate = (direct_sound_create *)
      GetProcAddress(DSoundLibrary, "DirectSoundCreate");

    LPDIRECTSOUND DirectSound;
    $(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &DirectSound, 0)),
      // both buffers use wave format
      WAVEFORMATEX WaveFormat    = {};
      WaveFormat.wFormatTag      = WAVE_FORMAT_PCM;
      WaveFormat.nChannels       = 2;
      WaveFormat.wBitsPerSample  = 16;
      WaveFormat.nSamplesPerSec  = SamplesPerSec;
      WaveFormat.nBlockAlign     = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8;
      WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;
      WaveFormat.cbSize          = 0;

      // init primary buffer
      $(SUCCEEDED(DirectSound->SetCooperativeLevel(Window, DSSCL_PRIORITY)),
        LPDIRECTSOUNDBUFFER PrimaryBuffer;
        DSBUFFERDESC BufferDescription = {};
        BufferDescription.dwSize     = SZ(BufferDescription);
        BufferDescription.dwFlags    = DSBCAPS_PRIMARYBUFFER;
        $(SUCCEEDED(DirectSound->CreateSoundBuffer(&BufferDescription, &PrimaryBuffer, 0)),
          HRESULT Error = PrimaryBuffer->SetFormat(&WaveFormat);
          $(SUCCEEDED(Error),
            OutputDebugStringA("Primary buffer set\n"))
          {/*TODO SetFormat fail*/})
        {/*TODO CreateSoundBuffer fail*/})
      {/*TODO Set Coop Levl fail*/}

      // init secondary buffer
      DSBUFFERDESC BufferDescription  = {};
      BufferDescription.dwSize        = SZ(BufferDescription);
      BufferDescription.dwFlags       = 0;
      BufferDescription.dwBufferBytes = BufferSize;
      BufferDescription.lpwfxFormat   = &WaveFormat;
      HRESULT Error = DirectSound->CreateSoundBuffer(&BufferDescription, &SecondaryBuffer, 0);
      $(SUCCEEDED(Error),
        OutputDebugStringA("Secondary buffer set\n"))
      {/*TODO create secondary buffer fail*/})
    {/*TODO DirectSound Create fail*/})
  {/*TODO No DirectSound Library*/}
}

INTERNAL win32_window_dimension
Win32GetWindowDimension(HWND Window) {
  win32_window_dimension Result;
  RECT ClientRect;
  GetClientRect(Window, &ClientRect);
  Result.Width  = ClientRect.right  - ClientRect.left;
  Result.Height = ClientRect.bottom - ClientRect.top;
  R(Result);
}

INTERNAL V
RenderWeirdGradient(win32_offscreen_buffer* Buffer, I x, I y) {
  uint8 *Row = (uint8 *)Buffer->Memory;
  i(Buffer->Height,
    uint32 *Pixel = (uint32*)Row;
    j(Buffer->Width,
      uint8 Blue  = y + i;
      uint8 Green = x + j;
      *Pixel++ = (Green << 8 ) | Blue);
    Row += Buffer->Pitch);
}

INTERNAL V
Win32ResizeDIBSection(win32_offscreen_buffer* Buffer, I w, I h) {
  $(Buffer->Memory, VirtualFree(Buffer->Memory, 0, MEM_RELEASE));

  I BytesPerPixel = 4;
  Buffer->Width   = w;
  Buffer->Height  = h;
  Buffer->Pitch   = w * BytesPerPixel;

  Buffer->Info.bmiHeader.biSize        =  SZ(Buffer->Info.bmiHeader);
  Buffer->Info.bmiHeader.biWidth       =  w;
  Buffer->Info.bmiHeader.biHeight      = -h; //negative for top-down instead of bot-up
  Buffer->Info.bmiHeader.biPlanes      =  1;
  Buffer->Info.bmiHeader.biBitCount    =  32;
  Buffer->Info.bmiHeader.biCompression =  BI_RGB;

  // NOTE Chris Hecker - BitBlt
  I BitmapMemorySize = BytesPerPixel * (Buffer->Width * Buffer->Height);
  Buffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

INTERNAL V
Win32DisplayBufferInWindow(win32_offscreen_buffer* Buffer, HDC DeviceContext,
                           I WindowWidth, I WindowHeight) {
  // TODO Aspect Ratio
  StretchDIBits(DeviceContext,
                0,0, WindowWidth, WindowHeight,
                0,0, Buffer->Width, Buffer->Height,
                Buffer->Memory,
                &Buffer->Info,
                DIB_RGB_COLORS, SRCCOPY);
}

INTERNAL LRESULT CALLBACK
Win32MainWindowCallback(HWND Window,
                        UINT Message,
                   WPARAM WParam,
                   LPARAM LParam)
{
  LRESULT Result = 0;
  S(Message,
    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_KEYDOWN:
    C(WM_KEYUP,
      B isAlt   =  (LParam & (1 << 29));
      B wasDown =  (LParam & (1 << 30));
      B isDown  = !(LParam & (1 << 31));
      uint32 VKCode = WParam;
      $(isAlt && VKCode == VK_F4, Running = false);
      $(isDown == wasDown, R(0)); // ignore repeats
      S(VKCode,
        C(VK_UP     , OutputDebugStringA(   "up\n"));
        C(VK_DOWN   , OutputDebugStringA( "down\n"));
        C(VK_LEFT   , OutputDebugStringA( "left\n"));
        C(VK_RIGHT  , OutputDebugStringA("right\n"));
        C(VK_ESCAPE ,
          $(isDown , OutputDebugStringA("esc is down"));
          $(wasDown, OutputDebugStringA("esc is up"));
          OutputDebugStringA("\n"));
        C(VK_SPACE, OutputDebugStringA("space\n"));
        C('Q', OutputDebugStringA("Q\n"));
        C('E', OutputDebugStringA("E\n"));
        C('W', OutputDebugStringA("W\n"));
        C('A', OutputDebugStringA("A\n"));
        C('S', OutputDebugStringA("S\n"));
        C('D', OutputDebugStringA("D\n"))));
    C(WM_DESTROY    , Running = false);
    C(WM_CLOSE       , Running = false);
    C(WM_ACTIVATEAPP , OutputDebugStringA("activate\n"));
    C(WM_PAINT       ,
      PAINTSTRUCT Paint;
      HDC DeviceContext = BeginPaint(Window, &Paint);
      win32_window_dimension Dimension = Win32GetWindowDimension(Window);
      Win32DisplayBufferInWindow(&GlobalBackBuffer, DeviceContext,
                                 Dimension.Width, Dimension.Height);
      EndPaint(Window, &Paint)),
    Result = DefWindowProc(Window, Message, WParam, LParam));
  R(Result);
}

I CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        I ShowCode)
{
  Win32LoadXInput();
  WNDCLASSA WindowClass = {};

  Win32ResizeDIBSection(&GlobalBackBuffer, 1280, 720);

  WindowClass.style         = CS_HREDRAW|CS_VREDRAW;
  WindowClass.lpfnWndProc   = Win32MainWindowCallback;
  WindowClass.hInstance     = Instance;
  WindowClass.lpszClassName = "The Window";

  $(RegisterClassA(&WindowClass),
    HWND Window =
      CreateWindowEx(0,
                     WindowClass.lpszClassName,
                     "Application",
                     WS_OVERLAPPEDWINDOW|WS_VISIBLE,
                     CW_USEDEFAULT, CW_USEDEFAULT,
                     CW_USEDEFAULT, CW_USEDEFAULT,
                     0, 0, Instance, 0);
    $(Window,
      HDC DeviceContext = GetDC(Window);
      Running = true; I xoff = 0; I yoff = 0;

      // sound buffer setup
      uint32 SampleIndex  = 0;
      int16  ToneVolume   = 1000;
      I SamplesPerSecond  = 40000;
      I hz                = 256;
      I SquareWavePeriod  = SamplesPerSecond / hz;
      I HalfPeriod        = SquareWavePeriod / 2;
      I BytesPerSample    = SZ(int16) * 2;
      I SecondaryBufferSize = SamplesPerSecond * BytesPerSample;
      Win32InitDSound(Window, SamplesPerSecond, SecondaryBufferSize);
      SecondaryBuffer->Play(0,0,DSBPLAY_LOOPING);

      W(Running,
        MSG Message;

        W(PeekMessage(&Message, 0, 0, 0, PM_REMOVE),
          $(WM_QUIT == Message.message, Running = false);
          TranslateMessage(&Message);
          DispatchMessage(&Message));

        i((DWORD)XUSER_MAX_COUNT,
          XINPUT_STATE ControllerState;
          $(XInputGetState(i, &ControllerState) == ERROR_SUCCESS,
            XINPUT_GAMEPAD *Pad = &ControllerState.Gamepad;
            B Up    = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_UP);
            B Down  = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
            B Left  = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
            B right = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
            B Start = (Pad->wButtons & XINPUT_GAMEPAD_START);
            B Back  = (Pad->wButtons & XINPUT_GAMEPAD_BACK);
            B LS    = (Pad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
            B RS    = (Pad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
            B Abtn  = (Pad->wButtons & XINPUT_GAMEPAD_A);
            B Bbtn  = (Pad->wButtons & XINPUT_GAMEPAD_B);
            B Xbtn  = (Pad->wButtons & XINPUT_GAMEPAD_X);
            B Ybtn  = (Pad->wButtons & XINPUT_GAMEPAD_Y);
            int16 stickx = Pad->sThumbLX;
            int16 sticky = Pad->sThumbLY;
            xoff += stickx >> 12;
            yoff -= sticky >> 12));


        RenderWeirdGradient(&GlobalBackBuffer, xoff, yoff);

        /* AUDIO SECTION */
        DWORD PlayCursor, WriteCursor;
        $(SUCCEEDED(SecondaryBuffer->GetCurrentPosition(&PlayCursor, &WriteCursor)),

          DWORD ByteToLock = (SampleIndex * BytesPerSample) % SecondaryBufferSize;
          DWORD BytesToWrite;

          $(ByteToLock > PlayCursor,
            BytesToWrite = SecondaryBufferSize - ByteToLock;
            BytesToWrite += PlayCursor)
          { BytesToWrite = PlayCursor - ByteToLock;}


          V     *Region1    , *Region2;
          DWORD  Region1Size,  Region2Size;
          $(SUCCEEDED(SecondaryBuffer->Lock(ByteToLock, BytesToWrite,
                                &Region1, &Region1Size,
                                &Region2, &Region2Size,
                                0)),

            int16* SampleOut = (int16*)Region1;
            DWORD Region1SampleCount = Region1Size / BytesPerSample;
            DWORD Region2SampleCount = Region2Size / BytesPerSample;
            i(Region1SampleCount,
              int16 SampleValue = (SampleIndex++ / HalfPeriod) % 2 ? ToneVolume : -ToneVolume;
              *SampleOut++ = SampleValue;
              *SampleOut++ = SampleValue);

            SampleOut = (int16*)Region2;
            i(Region2SampleCount,
              int16 SampleValue = (SampleIndex++ / HalfPeriod) % 2? ToneVolume : -ToneVolume;
              *SampleOut++ = SampleValue;
              *SampleOut++ = SampleValue);

            SecondaryBuffer->Unlock(Region1, Region1Size, Region2, Region2Size)));
        /* END AUDIO SECTION */


        win32_window_dimension Dimension = Win32GetWindowDimension(Window);
        Win32DisplayBufferInWindow(&GlobalBackBuffer, DeviceContext,
                                   Dimension.Width, Dimension.Height);

        ReleaseDC(Window, DeviceContext);
        ++xoff)));
  R(0);
}
