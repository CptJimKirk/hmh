#ifndef MY_H_
#define MY_H_

// Types are simply mapped to shorter descriptions
#define I  int
#define V  void
#define TY decltype
#define SZ sizeof
#define B  bool
#define R  return

/*
 * It is worth spending some time simplifying the for loop macro expression
 * In case you aren't familiar ... is a stand in for variadic arguments
 * Windows refers to these with __VA_ARGS__
 * You can have multiple statements in the place of the ... form
 * so ij might be formatted like this:
 * ij(x, 5, statement);
 *
 * or like this
 * ij(x, 5, statement1; statement2; statement3);
 *
 * Notice the last statement in  both examples has no semicolon in the parens
 * as that is supplied in the macro expansion
 *
 * Keep in mind macros are only text replacement
 * so TY is literally decltype. TY(n) ←→ decltype(n)
 * if n is an int like 5, then this would expand to
 * int n_=(5) // parens are only here in case n is an expression
 * int n_=(1 + 4)
 *
 * Using this as an example
 * ij(x, 5, int a = 1; int b = 2; printf("%d\n", a + b + x));
 * the expansion is thus:
 *
 * for(int n_=(5),x=0;x<n;++x){int a = 1; int b = 2; printf("%d\n", a + b + x);}
*/
#define ij(i,n,...) for(TY(n)n_=(n),i=0;i<n_;++i){__VA_ARGS__;}

/* i and j macros simply supply the varnames i and j
 *
 * example:
 * i(5, printf("%d\n", i));
 *
 * nested example with multiple statements
 * i(5, int double = 2*i; j(5, printf("%d\n", double + j)));
 *
 *  see line 36 in win32_handmade.cpp for nested example in the project
*/
#define i(n,...)    ij(i,n, __VA_ARGS__)
#define j(n,...)    ij(j,n, __VA_ARGS__)

// while, ... follows the same rules as the for loop
#define W(c,...) while((c)){__VA_ARGS__;}

/*
 * same rules apply to varargs here
 * but if you don't want to use else you can simply end with ;
 * $(true, statements);
 * $(false, statements) {
 *     // regular else block
 * }
 */
#define $(b,...) if(b){__VA_ARGS__;}else

/*
 * c can be multiple statements
 * S(message,
 *   C(WM_SIZE , statements);
 *   C(WM_THING, statements);
 *   C(WM_STUFF, statements);
 *   C(WM_WHAT , statements);
 *   C(WM_HUH  , statements);
 *   //default block
 *   statement1;
 *   statement2);
 */
#define C(i,...)   case i:{__VA_ARGS__;}break;
#define S(i,c,...) switch(i){c default:__VA_ARGS__;}

#endif // MY_H_
